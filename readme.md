# Exercise 14 result

```
./mem
(malloc): TIME FOR ALLOCATION 1 bytes = 0.000035 seconds
(calloc): TIME FOR ALLOCATION 1 bytes = 0.000005 seconds
(alloca): TIME FOR ALLOCATION 1 bytes = 0.000002 seconds


(malloc): TIME FOR ALLOCATION 2 bytes = 0.000003 seconds
(calloc): TIME FOR ALLOCATION 2 bytes = 0.000003 seconds
(alloca): TIME FOR ALLOCATION 2 bytes = 0.000002 seconds


(malloc): TIME FOR ALLOCATION 4 bytes = 0.000003 seconds
(calloc): TIME FOR ALLOCATION 4 bytes = 0.000002 seconds
(alloca): TIME FOR ALLOCATION 4 bytes = 0.000002 seconds


(malloc): TIME FOR ALLOCATION 8 bytes = 0.000003 seconds
(calloc): TIME FOR ALLOCATION 8 bytes = 0.000002 seconds
(alloca): TIME FOR ALLOCATION 8 bytes = 0.000003 seconds


(malloc): TIME FOR ALLOCATION 16 bytes = 0.000003 seconds
(calloc): TIME FOR ALLOCATION 16 bytes = 0.000003 seconds
(alloca): TIME FOR ALLOCATION 16 bytes = 0.000002 seconds


(malloc): TIME FOR ALLOCATION 32 bytes = 0.000003 seconds
(calloc): TIME FOR ALLOCATION 32 bytes = 0.000003 seconds
(alloca): TIME FOR ALLOCATION 32 bytes = 0.000003 seconds


(malloc): TIME FOR ALLOCATION 64 bytes = 0.000003 seconds
(calloc): TIME FOR ALLOCATION 64 bytes = 0.000003 seconds
(alloca): TIME FOR ALLOCATION 64 bytes = 0.000002 seconds


(malloc): TIME FOR ALLOCATION 128 bytes = 0.000002 seconds
(calloc): TIME FOR ALLOCATION 128 bytes = 0.000003 seconds
(alloca): TIME FOR ALLOCATION 128 bytes = 0.000002 seconds


(malloc): TIME FOR ALLOCATION 256 bytes = 0.000003 seconds
(calloc): TIME FOR ALLOCATION 256 bytes = 0.000004 seconds
(alloca): TIME FOR ALLOCATION 256 bytes = 0.000002 seconds


(malloc): TIME FOR ALLOCATION 512 bytes = 0.000002 seconds
(calloc): TIME FOR ALLOCATION 512 bytes = 0.000002 seconds
(alloca): TIME FOR ALLOCATION 512 bytes = 0.000002 seconds


(malloc): TIME FOR ALLOCATION 1024 bytes = 0.000011 seconds
(calloc): TIME FOR ALLOCATION 1024 bytes = 0.000002 seconds
(alloca): TIME FOR ALLOCATION 1024 bytes = 0.000002 seconds


(malloc): TIME FOR ALLOCATION 2048 bytes = 0.000004 seconds
(calloc): TIME FOR ALLOCATION 2048 bytes = 0.000004 seconds
(alloca): TIME FOR ALLOCATION 2048 bytes = 0.000002 seconds


(malloc): TIME FOR ALLOCATION 4096 bytes = 0.000010 seconds
(calloc): TIME FOR ALLOCATION 4096 bytes = 0.000002 seconds
(alloca): TIME FOR ALLOCATION 4096 bytes = 0.000003 seconds


(malloc): TIME FOR ALLOCATION 8192 bytes = 0.000009 seconds
(calloc): TIME FOR ALLOCATION 8192 bytes = 0.000004 seconds
(alloca): TIME FOR ALLOCATION 8192 bytes = 0.000003 seconds


(malloc): TIME FOR ALLOCATION 16384 bytes = 0.000011 seconds
(calloc): TIME FOR ALLOCATION 16384 bytes = 0.000009 seconds
(alloca): TIME FOR ALLOCATION 16384 bytes = 0.000006 seconds


(malloc): TIME FOR ALLOCATION 32768 bytes = 0.000006 seconds
(calloc): TIME FOR ALLOCATION 32768 bytes = 0.000019 seconds
(alloca): TIME FOR ALLOCATION 32768 bytes = 0.000012 seconds


(malloc): TIME FOR ALLOCATION 65536 bytes = 0.000009 seconds
(calloc): TIME FOR ALLOCATION 65536 bytes = 0.000053 seconds
(alloca): TIME FOR ALLOCATION 65536 bytes = 0.000009 seconds


(malloc): TIME FOR ALLOCATION 131072 bytes = 0.000044 seconds
(calloc): TIME FOR ALLOCATION 131072 bytes = 0.000115 seconds
(alloca): TIME FOR ALLOCATION 131072 bytes = 0.000012 seconds


(malloc): TIME FOR ALLOCATION 262144 bytes = 0.000009 seconds
(calloc): TIME FOR ALLOCATION 262144 bytes = 0.000221 seconds
(alloca): TIME FOR ALLOCATION 262144 bytes = 0.000009 seconds


(malloc): TIME FOR ALLOCATION 524288 bytes = 0.000038 seconds
(calloc): TIME FOR ALLOCATION 524288 bytes = 0.000056 seconds
(alloca): TIME FOR ALLOCATION 524288 bytes = 0.000010 seconds


(malloc): TIME FOR ALLOCATION 1048576 bytes = 0.000033 seconds
(calloc): TIME FOR ALLOCATION 1048576 bytes = 0.000658 seconds
(alloca): TIME FOR ALLOCATION 1048576 bytes = 0.000012 seconds


(malloc): TIME FOR ALLOCATION 2097152 bytes = 0.000044 seconds
(calloc): TIME FOR ALLOCATION 2097152 bytes = 0.000895 seconds
(alloca): TIME FOR ALLOCATION 2097152 bytes = 0.000014 seconds


(malloc): TIME FOR ALLOCATION 4194304 bytes = 0.000045 seconds
(calloc): TIME FOR ALLOCATION 4194304 bytes = 0.001785 seconds
(alloca): TIME FOR ALLOCATION 4194304 bytes = 0.000015 seconds


(malloc): TIME FOR ALLOCATION 8388608 bytes = 0.000042 seconds
(calloc): TIME FOR ALLOCATION 8388608 bytes = 0.003362 seconds
make: *** [Makefile:6: test] Segmentation fault (core dumped)
```
