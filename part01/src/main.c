#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void alloca_printf(long i)
{
	clock_t begin, end;
	char* temp;
	double time_spent;

	begin = clock();
	temp = alloca(i);
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("(alloca): TIME FOR ALLOCATION %d bytes = %f seconds\n\n\n",i, time_spent);
}


int main(void)
{
	clock_t begin, end;
	char* temp;
	double time_spent;

	for(long i = 1; i >= 0; i = i * 2)
	{
		begin = clock();
		temp = malloc(i);
		free(temp);
		end = clock();
		time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
		printf("(malloc): TIME FOR ALLOCATION %d bytes = %f seconds\n",i, time_spent);

		begin = clock();
		temp = calloc(i,1);
		free(temp);
		end = clock();
		time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
		printf("(calloc): TIME FOR ALLOCATION %d bytes = %f seconds\n",i, time_spent);

		alloca_printf(i);

	}
	return 0;
}
